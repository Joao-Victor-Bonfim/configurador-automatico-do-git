@echo off
echo Removendo o e-mail e nome contidos em user em todos os domi'nios,
echo ou seja, system, global e user.
echo Removendo do domi'nio de sistema.
git config --system --unset user.name
git config --system --unset user.email
echo Removendo do domi'nio global.
git config --global --unset user.name
git config --global --unset user.email
echo Removendo do domi'nio local.
git config --local --unset user.name
git config --local --unset user.email
pause
echo Configurando o git, no domi'nio local
echo (os dados sera~o deletados ao deletar o arquivo .git contido na pasta
echo do reposito'rio, ou seja, e' so deletar a pasta do reposito'rio),
echo para os seus para^metros.
set /p nome="Insira seu nome: "
set /p email="Insira seu e-mail: "
git config --local user.name "%nome%"
git config --local user.email "%email%"
pause
echo Dando unset no gerenciador de credenciais para que as suas credenciais
echo para sites de reposito'rio (i.e.: Gitlab, GitHub e BitBucket) na~o
echo sejam salvas no computador por uma aplicac,a~o rodando em paralelo.
git config --system --unset credential.manager
git config --global --unset credential.manager
git config --local --unset credential.manager
pause
cls
echo Obrigado pela atenc,a~o!
echo Meu trabalho esta' conclui'do por aqui.
echo Tenha um bom dia!
pause
exit
